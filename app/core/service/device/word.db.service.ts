import { Injectable } from '@angular/core';
import { TABLE_WORD_FAILED } from './db.constants';
import { DbService } from './db.service';
import { mapCollection } from '~/core/utils/collection.util';
import { formatDate } from '~/core/utils/date.util';

@Injectable()
export class WordDbService {

  constructor(private dbService: DbService) { }

  async saveTestResponse(category: number, word: number, failure: number) {
    const db = await this.dbService.getDbConnection();
    try {
      await db.execSQL(`INSERT INTO ${ TABLE_WORD_FAILED } (category_id, word_id, failure, date) VALUES (?, ?, ?, date('now'))`, [ category, word, failure ]); // tslint:disable-line:max-line-length
    } catch (error) {
      console.error('Error inserting test response', error);
    } finally {
      db.close();
    }
  }

  async getTestsResponses() {
    const db = await this.dbService.getDbConnection();
    try {
      const res = await db
        .all(`SELECT category_id, word_id, COUNT(1) as responses, SUM(failure) FROM ${ TABLE_WORD_FAILED } GROUP BY category_id, word_id`, []); // tslint:disable-line:max-line-length

      return mapCollection(row => ({ categoryId: row[0], wordId: row[1], responses: row[2], failures: row[3] }), res);
    } finally {
      db.close();
    }
  }

  async getTestsResponsesBetweenDates(start: number, end: number) {
    const db = await this.dbService.getDbConnection();
    try {
      const startDate = formatDate(start, 'YYYY-MM-DD');
      const endDate = formatDate(end, 'YYYY-MM-DD');
      /* tslint:disable:max-line-length */
      const res = await db
        .all(`SELECT category_id, word_id, COUNT(1) as responses, SUM(failure) as failures FROM ${ TABLE_WORD_FAILED } WHERE date >= '${ startDate }' AND date <= '${ endDate }' GROUP BY category_id, word_id`, []);
      /* tslint:enable:max-line-length */

      return mapCollection(row => ({ categoryId: row[0], wordId: row[1], responses: row[2], failures: row[3] }), res);
    } finally {
      db.close();
    }
  }
}
