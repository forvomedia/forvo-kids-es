import { Injectable } from '@angular/core';
import * as platformModule from 'tns-core-modules/platform';

@Injectable()
export class DeviceService {

  private screenSizeAlreadyShown: number;

  constructor() {
    this.screenSizeAlreadyShown = 0;
  }

  deviceIsAndroid() {
    return (typeof(platformModule.isAndroid) === 'boolean') ? platformModule.isAndroid : false;
  }

  deviceIsIOS() {
    return (typeof(platformModule.isIOS) === 'boolean') ? platformModule.isIOS : false;
  }

  deviceIsPhone() {
    if (platformModule.device.deviceType === 'Phone') {
      return true;
    } else {
      return false;
    }
  }

  deviceIsIOSPhoneFive() {
    if (this.deviceIsIOS() && this.deviceIsPhone && platformModule.screen.mainScreen.widthDIPs === 320) {
      return true;
    } else {
      return false;
    }
  }

  deviceIsIOSPhonePlus() {
    if (this.deviceIsIOS() && this.deviceIsPhone && platformModule.screen.mainScreen.widthDIPs === 414) {
      return true;
    } else {
      return false;
    }
  }

  deviceIsIOSTablet1024() {
    if (this.deviceIsIOS() && !this.deviceIsPhone() && platformModule.screen.mainScreen.widthDIPs >= 1024) {
      return true;
    } else {
      return false;
    }
  }

  deviceIsIPhoneX() {
    if (this.deviceIsIOS() && platformModule.screen.mainScreen.heightPixels === 2436 &&
    platformModule.screen.mainScreen.widthPixels === 1125) {
      return true;
    } else {
      return false;
    }
  }

  showScreenSize() {
    if (!this.screenSizeAlreadyShown) {
      console.log('DIPs: ' + platformModule.screen.mainScreen.widthDIPs + 'x' + platformModule.screen.mainScreen.heightDIPs);
      console.log('Pixels: ' + platformModule.screen.mainScreen.widthPixels + 'x' + platformModule.screen.mainScreen.heightPixels);
      this.screenSizeAlreadyShown = 1;
    }
  }
}
