import { of } from 'rxjs/observable/of';
import { GameItemService } from '~/core/service/game-item.service';
import { mapCollection } from '~/core/utils/collection.util';
import { DATA_FOLDER } from '../config/constants';
import { Category } from '../model/category';
import { readJson } from '../utils/file.util';

export class CategoryService {
  private categories: Category[];

  constructor() {
    const categoryFile = `${ DATA_FOLDER }categories.json`;
    this.categories = readJson(categoryFile).map(
        (json) => new Category(+json.id, json.name, json.image, json.audio, json.items, json.folder )
      );
  }

  getCategories() {
    return of(this.categories);
  }

  getCategoriesWithWords() {
    const categoriesWithWords = mapCollection(category => {
      const cat = category.clone();
      cat.words = GameItemService.getWordsByCategory(cat);

      return cat;
    }, this.categories);

    return of(categoriesWithWords);
  }

  getCategoryById(categoryId: number) {
    return of(this.categories.find(category => category.id === categoryId));
  }
}
