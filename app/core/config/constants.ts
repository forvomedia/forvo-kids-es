const BASE_FOLDER = 'assets/';

export const IMAGE_FOLDER = `~/${ BASE_FOLDER }images/`;

export const AUDIO_FOLDER = `${ BASE_FOLDER }audios/`;

export const DATA_FOLDER = `${ BASE_FOLDER }data/`;

export const IMAGE_CATEGORY_FOLDER = `${ IMAGE_FOLDER }category/`;

export const IMAGE_GAME_ITEM_FOLDER = `${ IMAGE_FOLDER }game-item/`;

export const AUDIO_CATEGORY_FOLDER = `${ AUDIO_FOLDER }category/`;

export const AUDIO_GAME_ITEM_FOLDER = `${ AUDIO_FOLDER }game-item/`;

export const HOME_IMAGE = `${ IMAGE_FOLDER }home.png`;

export const HOME_AUDIO = `${ AUDIO_FOLDER }home.mp3`;

export const GAME_RESULT_IMAGE = `${ IMAGE_FOLDER }game_result.png`;

export const GAME_RESULT_AUDIO = `${ AUDIO_FOLDER }game_result.mp3`;

export const START_AUDIO = `${ AUDIO_FOLDER }start.mp3`;

export const ERROR_AUDIO = `${ AUDIO_FOLDER }test_error.mp3`;

export const SUCCESS_AUDIO = `${ AUDIO_FOLDER }test_success.mp3`;

export const ICON_ERROR_IMAGE = `${ IMAGE_FOLDER }icon_error.png`;

export const ICON_SUCCESS_IMAGE = `${ IMAGE_FOLDER }icon_success.png`;

export const STARS_3_AUDIO = `${ AUDIO_FOLDER }level_complete_stars_3.mp3`;

export const STARS_2_AUDIO = `${ AUDIO_FOLDER }level_complete_stars_2.mp3`;

export const STARS_1_AUDIO = `${ AUDIO_FOLDER }level_complete_stars_1.mp3`;

export const ICON_STAR_EMPTY_IMAGE = `${ IMAGE_FOLDER }icon_star_empty.png`;

export const ICON_STAR_FULL_IMAGE = `${ IMAGE_FOLDER }icon_star_full.png`;

export const ICON_SOUND_IMAGE = `${ IMAGE_FOLDER }icon_sound.png`;

export const ICON_FORVOKIDS = `${ IMAGE_FOLDER }icon_forvokids.png`;

export const TRANSLATION_GAME_START = 'translation_game_start';

export const TRANSLATION_GAME_RESULT_TRY_AGAIN = 'translation_game_result_try_again';

export const TRANSLATION_GAME_RESULT_WELL_DONE = 'translation_game_result_well_done';

export const TRANSLATION_GAME_RESULT_VERY_GOOD = 'translation_game_result_very_good';

export const TRANSLATION_STATS_SELECT_PERIOD = 'translation_stats_select_period';

export const TRANSLATION_STATS_TOTAL = 'translation_stats_total';

export const TRANSLATION_STATS_NO_STATS = 'translation_stats_no_stats';

export const TRANSLATION_STATS_CLOSE = 'translation_stats_close';

export const TRANSLATION_PARENTAL_INFORMATION = 'translation_parental_information';

export const TRANSLATION_LESS_PLAYED_CATEGORIES = 'translation_less_played_categories';

export const TRANSLATION_MOST_FAILED_WORDS = 'translation_most_failed_words';

export const TRANSLATION_INFORMATION = 'translation_information';

export const TRANSLATION_INFORMATION_TITLE = 'translation_information_title';

export const TRANSLATION_INFORMATION_PARAGRAPH_1 = 'translation_information_paragraph_1';

export const TRANSLATION_INFORMATION_PARAGRAPH_2 = 'translation_information_paragraph_2';

export const TRANSLATION_INFORMATION_PARAGRAPH_3 = 'translation_information_paragraph_3';

export const TRANSLATION_INFORMATION_PARAGRAPH_4 = 'translation_information_paragraph_4';

export const TRANSLATION_INFORMATION_PARAGRAPH_5 = 'translation_information_paragraph_5';

export const TRANSLATION_INFORMATION_PARAGRAPH_6 = 'translation_information_paragraph_6';

export const TRANSLATION_INFORMATION_FOOTER = 'translation_information_footer';

export const TRANSLATION_INFORMATION_ADDRESS = 'translation_information_address';
