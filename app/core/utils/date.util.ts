import * as moment from 'moment';
import {capitalizeFirstLetter} from '~/core/utils/string.util';

export const getCurrentMillis = () => moment().locale('es').valueOf();

export const subtractMonthsToDate = (date: number, months: number) => {
    return moment(date).locale('es').subtract(months, 'month').valueOf();
};

export const formatDate = (date: number, format: string) =>
capitalizeFirstLetter(moment(date).locale('es').format(format));

export const getFirstMillisOfMonth = (date: number) => moment(date).locale('es').startOf('month').valueOf();

export const getLastMillisOfMonth = (date: number) => moment(date).locale('es').endOf('month').valueOf();
