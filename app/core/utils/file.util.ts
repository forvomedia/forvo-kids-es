import * as fs from 'file-system';

export const getAbsolutePath = (file: string) => fs.path.join(fs.knownFolders.currentApp().path, file);

export const checkFile = (file: string) => fs.File.exists(getAbsolutePath(file));

export const readFile = (file: string) => (checkFile(file))
  ? fs.knownFolders.currentApp().getFile(file).readTextSync()
  : null;

export const readJson = (file: string) => JSON.parse(readFile(file));
