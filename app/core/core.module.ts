import { NgModule } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { CategoryService } from './service/category.service';
import { CategoryDbService } from './service/device/category.db.service';
import { DbService } from './service/device/db.service';
import { WordDbService } from './service/device/word.db.service';
import { GameItemService } from './service/game-item.service';
import { PlayerService } from './service/player.service';
import { TranslationService } from '~/core/service/translation.service';

@NgModule({
  imports: [],
  exports: [
    NativeScriptModule
  ],
  providers: [
    TranslationService,
    CategoryService,
    GameItemService,
    PlayerService,
    // Device
    DbService,
    CategoryDbService,
    WordDbService
  ]
})
export class CoreModule { }
