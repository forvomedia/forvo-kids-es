import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
/* tslint:disable:max-line-length */
import { STARS_1_AUDIO, STARS_2_AUDIO, STARS_3_AUDIO, TRANSLATION_GAME_RESULT_TRY_AGAIN, TRANSLATION_GAME_RESULT_VERY_GOOD, TRANSLATION_GAME_RESULT_WELL_DONE } from '../../../core/config/constants';
/* tslint:enable:max-line-length */
import { GameService } from '../../game.service';
import { DeviceService } from '../../../core/service/device.service';
import { TranslationService } from '~/core/service/translation.service';

@Component({
  selector: 'app-game-result',
  moduleId: module.id,
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.css']
})
export class GameResultComponent implements OnInit {
  stars: number;
  translations: string[];
  translation_game_result_try_again: string;
  translation_game_result_well_done: string;
  translation_game_result_very_good: string;

  constructor(
    private gameService: GameService,
    private deviceService: DeviceService,
    private translationService: TranslationService,
    public routerExtensions: RouterExtensions
  ) {
    const translationIds = [
      TRANSLATION_GAME_RESULT_TRY_AGAIN,
      TRANSLATION_GAME_RESULT_WELL_DONE,
      TRANSLATION_GAME_RESULT_VERY_GOOD
    ];
    this.translation_game_result_try_again = TRANSLATION_GAME_RESULT_TRY_AGAIN;
    this.translation_game_result_well_done = TRANSLATION_GAME_RESULT_WELL_DONE;
    this.translation_game_result_very_good = TRANSLATION_GAME_RESULT_VERY_GOOD;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.stars = Math.floor(this.gameService.currentGame.score * 2) + 1;
    this.playAudio();
    this.gameService.clearCurrentCategoryCroups();
  }

  private playAudio() {
    const audio = (this.stars === 3) ? STARS_3_AUDIO : (this.stars > 1) ? STARS_2_AUDIO : STARS_1_AUDIO;
    this.gameService.play(audio);
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
