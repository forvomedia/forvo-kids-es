import { Component, Input } from '@angular/core';
import { ICON_STAR_EMPTY_IMAGE, ICON_STAR_FULL_IMAGE } from '../../../../core/config/constants';

@Component({
  selector: 'app-game-result-stars',
  moduleId: module.id,
  templateUrl: './game-result-stars.component.html',
  styleUrls: ['./game-result-stars.component.css']
})
export class GameResultStarsComponent {

  @Input() stars: Number | null;

  emptyStarImage() {
    return `${ ICON_STAR_EMPTY_IMAGE }`;
  }

  fullStarImage() {
    return `${ ICON_STAR_FULL_IMAGE }`;
  }

  get firstStarImage() {
    if (this.stars === null) {
      return this.emptyStarImage();
    } else {
      return this.fullStarImage();
    }
  }

  get secondStarImage() {
    if (this.stars === null || this.stars < 2) {
      return this.emptyStarImage();
    } else {
      return this.fullStarImage();
    }
  }

  get thirdStarImage() {
    if (this.stars === null || this.stars < 3) {
      return this.emptyStarImage();
    } else {
      return this.fullStarImage();
    }
  }
}
