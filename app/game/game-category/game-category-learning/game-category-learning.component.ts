import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameItem } from '../../../core/model/game-item';
import { GameService } from '../../game.service';

@Component({
  selector: 'app-game-category-learning',
  moduleId: module.id,
  templateUrl: './game-category-learning.component.html',
  styleUrls: ['./game-category-learning.component.css']
})
export class GameCategoryLearningComponent implements OnInit {
  itemIndex = 0;
  items: GameItem[];

  constructor(
    private gameService: GameService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    const categoryId = +this.activeRoute.parent.parent.snapshot.params['id'];

    if (!this.gameService.currentCategoryGroups || this.gameService.currentCategoryGroups === null) {
      this.gameService.getGroupsOfItemsByCategory(categoryId)
        .then((categoryGroups) => {
          this.setGroupsOfItems(categoryId, categoryGroups);
        });
    } else {
      if ((categoryId === this.gameService.currentCategoryGroups.categoryId) &&
      this.gameService.currentCategoryGroups.started === 1 &&
      this.gameService.currentCategoryGroups.currentGroup < this.gameService.currentCategoryGroups.length - 1) {
        this.gameService.currentCategoryGroups.currentGroup++;
        this.items = this.gameService.currentCategoryGroups.groups[this.gameService.currentCategoryGroups.currentGroup];
      } else {
        this.gameService.getGroupsOfItemsByCategory(categoryId)
        .then((categoryGroups) => {
          this.setGroupsOfItems(categoryId, categoryGroups);
        });
      }
    }
  }

  setGroupsOfItems(categoryId: number, categoryGroups: any[]) {
    if (categoryGroups) {
      this.gameService.currentCategoryGroups = {
        categoryId: categoryId,
        groups: categoryGroups,
        currentGroup: 0,
        length: categoryGroups.length,
        started: 0
      };
      this.items = categoryGroups[0];
    }
  }

  get item() {
    return (this.items) ? this.items[this.itemIndex] : null;
  }

  goToNext() {
    if (this.itemIndex < this.items.length - 1) {
      this.itemIndex++;
    } else {
      this.router.navigate(['start'], { relativeTo: this.activeRoute.parent.parent });
    }
  }
}
