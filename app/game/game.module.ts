import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CategoryItemStarsComponent } from './game-categories/category-item/category-item-stars/category-item-stars.component';
import { CategoryItemComponent } from './game-categories/category-item/category-item.component';
import { GameCategoriesComponent } from './game-categories/game-categories.component';
import { GameCardComponent } from './game-category/game-category-learning/game-card/game-card.component';
import { GameCategoryLearningComponent } from './game-category/game-category-learning/game-category-learning.component';
import { GameCategoryComponent } from './game-category/game-category.component';
import { GameHeaderComponent } from './game-category/game-header/game-header.component';
import { GameResultComponent } from './game-category/game-result/game-result.component';
import { GameResultStarsComponent } from './game-category/game-result/game-result-stars/game-result-stars.component';
import { GameStartComponent } from './game-category/game-start/game-start.component';
import { GameTestCardsComponent } from './game-category/game-test/game-test-cards/game-test-cards.component';
import { GameTestComponent } from './game-category/game-test/game-test.component';
import { GameRoutingModule } from './game-routing.module';
import { GameService } from './game.service';
import { DeviceService } from '../core/service/device.service';

@NgModule({
  declarations: [
    CategoryItemComponent,
    CategoryItemStarsComponent,
    GameCardComponent,
    GameCategoriesComponent,
    GameCategoryComponent,
    GameCategoryLearningComponent,
    GameHeaderComponent,
    GameResultComponent,
    GameResultStarsComponent,
    GameStartComponent,
    GameTestCardsComponent,
    GameTestComponent
  ],
  imports: [
    GameRoutingModule,
    SharedModule
  ],
  providers: [
    GameService,
    DeviceService
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class GameModule { }
