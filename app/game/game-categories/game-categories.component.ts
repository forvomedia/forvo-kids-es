import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular';
import { TNSPlayer } from 'nativescript-audio';
import { Observable } from 'rxjs/Observable';
import { Page } from 'tns-core-modules/ui/page';
import { DeviceService } from '~/core/service/device.service';
import { HOME_AUDIO } from '../../core/config/constants';
import { Category } from '../../core/model/category';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-categories',
  moduleId: module.id,
  templateUrl: './game-categories.component.html',
  styleUrls: ['./game-categories.component.css']
})
export class GameCategoriesComponent implements OnInit {
  categories$: Observable<Category[]>;
  private player: TNSPlayer;

  constructor(
    private gameService: GameService,
    private deviceService: DeviceService,
    private page: Page,
    private router: Router,
    private activeRoute: ActivatedRoute,
    public routerExtensions: RouterExtensions
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }
  }

  ngOnInit() {
    this.page.addEventListener('navigatedTo', () => this.player = this.gameService.playOnLoop(HOME_AUDIO));
    this.categories$ = this.gameService.getCategories();
  }

  goToCategory(category: Category) {
    this.gameService.saveCategoryVisit(category.id);
    this.gameService.play(
      category.audio,
      () => this.router.navigate([ category.id ], { relativeTo: this.activeRoute })
    );
  }

  getScore(category: Category) {
    return this.gameService.results[category.id.toString()];
  }

  goToHome() {
    this.routerExtensions.backToPreviousPage();
  }
}
