import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { DeviceService } from './core/service/device.service';
import { HomeComponent } from './home/home.component';
import { TranslationService } from '~/core/service/translation.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  bootstrap: [AppComponent],
  imports: [
    AppRoutingModule,
    CoreModule
  ],
  providers: [
    DeviceService,
    TranslationService
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
