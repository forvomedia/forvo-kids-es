import { capitalizeFirstLetter } from '~/core/utils/string.util';

export class Stat {
  constructor(
    public title: string,
    public value: number,
    public subtitle?: string
  ) { }

  get formattedSubtitle() {
    const _subtitle: string = capitalizeFirstLetter(this.subtitle);
    return `(${_subtitle})`;
  }
}
