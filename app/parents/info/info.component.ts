import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { Page } from 'tns-core-modules/ui/page';
import { DeviceService } from '../../core/service/device.service';
import { TranslationService } from '~/core/service/translation.service';
/* tslint:disable:max-line-length */
import { TRANSLATION_INFORMATION, TRANSLATION_INFORMATION_TITLE, TRANSLATION_INFORMATION_PARAGRAPH_1, TRANSLATION_INFORMATION_PARAGRAPH_2, TRANSLATION_INFORMATION_PARAGRAPH_3, TRANSLATION_INFORMATION_PARAGRAPH_4, TRANSLATION_INFORMATION_PARAGRAPH_5, TRANSLATION_INFORMATION_PARAGRAPH_6, TRANSLATION_INFORMATION_FOOTER, TRANSLATION_INFORMATION_ADDRESS } from '~/core/config/constants';
/* tslint:enable:max-line-length */

@Component({
  selector: 'app-info',
  moduleId: module.id,
  templateUrl: './info.component.html',
  styleUrls: ['./info.common.css', './info.component.css']
})

export class InfoComponent {
  translations: string[];
  translation_information: string;
  translation_information_title: string;
  translation_information_paragraph_1: string;
  translation_information_paragraph_2: string;
  translation_information_paragraph_3: string;
  translation_information_paragraph_4: string;
  translation_information_paragraph_5: string;
  translation_information_paragraph_6: string;
  translation_information_footer: string;
  translation_information_address: string;

  constructor(
    private deviceService: DeviceService,
    private translationService: TranslationService,
    public routerExtensions: RouterExtensions,
    page: Page
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }

    const translationIds = [
      TRANSLATION_INFORMATION,
      TRANSLATION_INFORMATION_TITLE,
      TRANSLATION_INFORMATION_PARAGRAPH_1,
      TRANSLATION_INFORMATION_PARAGRAPH_2,
      TRANSLATION_INFORMATION_PARAGRAPH_3,
      TRANSLATION_INFORMATION_PARAGRAPH_4,
      TRANSLATION_INFORMATION_PARAGRAPH_5,
      TRANSLATION_INFORMATION_PARAGRAPH_6,
      TRANSLATION_INFORMATION_FOOTER,
      TRANSLATION_INFORMATION_ADDRESS
    ];
    this.translation_information = TRANSLATION_INFORMATION;
    this.translation_information_title = TRANSLATION_INFORMATION_TITLE;
    this.translation_information_paragraph_1 = TRANSLATION_INFORMATION_PARAGRAPH_1;
    this.translation_information_paragraph_2 = TRANSLATION_INFORMATION_PARAGRAPH_2;
    this.translation_information_paragraph_3 = TRANSLATION_INFORMATION_PARAGRAPH_3;
    this.translation_information_paragraph_4 = TRANSLATION_INFORMATION_PARAGRAPH_4;
    this.translation_information_paragraph_5 = TRANSLATION_INFORMATION_PARAGRAPH_5;
    this.translation_information_paragraph_6 = TRANSLATION_INFORMATION_PARAGRAPH_6;
    this.translation_information_footer = TRANSLATION_INFORMATION_FOOTER;
    this.translation_information_address = TRANSLATION_INFORMATION_ADDRESS;
    this.translations = this.translationService.getSectionTranslations(translationIds);
   }

  goToHome() {
    this.routerExtensions.backToPreviousPage();
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  deviceIsIOSTablet1024() {
    return this.deviceService.deviceIsIOSTablet1024();
  }

  deviceIsIOS() {
    return this.deviceService.deviceIsIOS();
  }

  deviceIsIPhoneX() {
    return this.deviceService.deviceIsIPhoneX();
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
