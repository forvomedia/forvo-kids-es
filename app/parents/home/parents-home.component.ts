import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular';
import { Page } from 'tns-core-modules/ui/page';
import { DeviceService } from '~/core/service/device.service';
import { PlayerService } from '~/core/service/player.service';
import { TranslationService } from '~/core/service/translation.service';
/* tslint:disable:max-line-length */
import { TRANSLATION_PARENTAL_INFORMATION, TRANSLATION_LESS_PLAYED_CATEGORIES, TRANSLATION_MOST_FAILED_WORDS, TRANSLATION_INFORMATION } from '~/core/config/constants';
/* tslint:enable:max-line-length */

@Component({
  selector: 'app-parents-home',
  moduleId: module.id,
  templateUrl: './parents-home.component.html',
  styleUrls: ['./parents-home.component.css']
})
export class ParentsHomeComponent implements OnInit {
  translations: string[];
  translation_parental_information: string;
  translation_less_played_categories: string;
  translation_most_failed_words: string;
  translation_information: string;

  constructor(
    public deviceService: DeviceService,
    private page: Page,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private playerService: PlayerService,
    private translationService: TranslationService,
    public routerExtensions: RouterExtensions
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }

    const translationIds = [
      TRANSLATION_PARENTAL_INFORMATION,
      TRANSLATION_LESS_PLAYED_CATEGORIES,
      TRANSLATION_MOST_FAILED_WORDS,
      TRANSLATION_INFORMATION
    ];
    this.translation_parental_information = TRANSLATION_PARENTAL_INFORMATION;
    this.translation_less_played_categories = TRANSLATION_LESS_PLAYED_CATEGORIES;
    this.translation_most_failed_words = TRANSLATION_MOST_FAILED_WORDS;
    this.translation_information = TRANSLATION_INFORMATION;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.page.addEventListener('navigatedTo', () => this.playerService.stop());
  }

  goToHome() {
    this.routerExtensions.backToPreviousPage();
  }

  goToInfo() {
    this.router.navigate(['info'], { relativeTo: this.activeRoute });
  }

  goToLessPlayedCategories() {
    this.router.navigate(['categories'], { relativeTo: this.activeRoute });
  }

  goToMostFailedWords() {
    this.router.navigate(['words'], { relativeTo: this.activeRoute });
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
