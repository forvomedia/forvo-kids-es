import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ParentsService } from '~/parents/parents.service';
import { CategoriesStatsComponent } from '~/parents/stats/categories/categories-stats.component';
import { StatItemComponent } from '~/parents/stats/stats-list/stat-item/stat-item.component';
import { StatsListComponent } from '~/parents/stats/stats-list/stats-list.component';
import { WordsStatsComponent } from '~/parents/stats/words/words-stats.component';
import { SharedModule } from '~/shared/shared.module';
import { ParentsHomeComponent } from './home/parents-home.component';
import { InfoComponent } from './info/info.component';
import { ParentsRoutingModule } from './parents-routing.module';

@NgModule({
  declarations: [
    CategoriesStatsComponent,
    InfoComponent,
    ParentsHomeComponent,
    StatItemComponent,
    StatsListComponent,
    WordsStatsComponent
  ],
  imports: [
    ParentsRoutingModule,
    SharedModule
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
  providers: [
    ParentsService
  ]
})
export class ParentsModule { }
